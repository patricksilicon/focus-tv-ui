$(document).ready(function () {

    var poll_id=$('#poll_id').val();
    var getUrl = window.location;

    //get the base url
    var base_url =getUrl .protocol + "//" +getUrl.host;

    $.getJSON(base_url+"/poll-get-result/"+poll_id,
        function (j) {
            var a=[];
            var b=[];

            for (var i = 0; i < j.length; i++) {
                b[i]= j[i]['results'];
                a[i]= j[i]['answer'];
            }

            var options = {
                animationEnabled: true,
                title: {
                    text: "Poll Results"
                },
                axisY: {
                    title: "Results (in %)",
                    suffix: "%",
                    includeZero: true
                },
                axisX: {
                    title: "Results"
                },
                data: [{
                    type: "column",
                    yValueFormatString: "#,##0.0#" % "%",
                    dataPoints: [
                            {label: a[0], y: b[0]},
                            {label: a[1], y: b[1]},
                            {label: a[2], y: b[2]}
                            ]
                }]
            };
            $("#chartContainer").CanvasJSChart(options);
        })

});
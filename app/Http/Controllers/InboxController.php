<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CheckRole;
use App\Inbox;
use App\Keyword;
use App\Outbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InboxController extends Controller
{
    public function index(){

//        $inbox=Inbox::where('short_code','22644')
//            ->orderBy('id','desc')
//            ->get();
//        if (checkRole(['Admin'])) {
//           return "admin";
//        }

        $keywords=Keyword::get();


        return view('message.inbox',compact('keywords'));
    }

    //ajax inbox
    public function ajaxInbox(Request $request){

        $columns = array(
            0 => 'message_date',
            1 => 'msisdn',
            2 => 'status',
            3 => 'message',
        );

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $keyword=$request->keyword;

        $date=$request->date;



        //check keyword for all
        if($keyword=="all" && $date=="null"){

            $totalData=Inbox::count();

            $totalFiltered = $totalData;

            if (empty($request->input('search.value'))) {


                $posts=Inbox::orderBy($order, $dir)
                    ->offset($start)
                    ->limit($limit)
                    ->get();


            }else{

                $search = $request->input('search.value');


                $posts=Inbox::where('msisdn','LIKE',"%{$search}%")
                    ->orWhere('message', 'LIKE', "%{$search}%")
                    ->orWhere('message_date', 'LIKE', "%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();


                $totalFiltered=Inbox::count();



            }

        }
        elseif ($keyword!="all" && $date=="null"){

            $totalData=Inbox::where('keyword_id',$keyword)->count();

            $totalFiltered = $totalData;

            if (empty($request->input('search.value'))) {


                $posts=Inbox::where('keyword_id',$keyword)->orderBy($order, $dir)->get();


            }else{

                $search = $request->input('search.value');


                $posts=Inbox::where([['msisdn','LIKE',"%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message_date', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();


                $totalFiltered=Inbox::where([['msisdn','LIKE',"%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message_date', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->count();



            }


        }

        elseif ($keyword=="all" && $date!="null"){

            $totalData=Inbox::whereDate('created',$date)->count();

            $totalFiltered = $totalData;

            if (empty($request->input('search.value'))) {

                $posts=Inbox::whereDate('created',$date)->orderBy($order, $dir)->get();

            }else{

                $search = $request->input('search.value');


                $posts=Inbox::where([['msisdn','LIKE',"%{$search}%"],['keyword_id',$keyword]])
                   // ->whereDate('created',$date)
                    ->orWhere([['message', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message_date', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();


                $totalFiltered=Inbox::where([['msisdn','LIKE',"%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message_date', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->count();



            }


        }

        elseif ($keyword!="all" && $date!="null"){

            $totalData=Inbox::where('keyword_id',$keyword)
                ->whereDate('created',$date)
                ->count();

            $totalFiltered = $totalData;

            if (empty($request->input('search.value'))) {


                $posts=Inbox::where('keyword_id',$keyword)
                    ->whereDate('created',$date)
                    ->orderBy($order, $dir)
                    ->get();


            }else{

                $search = $request->input('search.value');


                $posts=Inbox::where([['msisdn','LIKE',"%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message_date', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();


                $totalFiltered=Inbox::where([['msisdn','LIKE',"%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->orWhere([['message_date', 'LIKE', "%{$search}%"],['keyword_id',$keyword]])
                    ->count();



            }


        }






        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {


                $nestedData['message_date'] = $post->message_date;
                $nestedData['message'] = $post->message;
                $nestedData['status'] = $post->status;
                $nestedData['msisdn'] = $post->msisdn;


                $data[] = $nestedData;

            }

            $json_data = array(
                "draw" => intval($request->input('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data" => $data
            );

            echo json_encode($json_data);
        }
    }


    public function outbox(){

        return view('message.outbox');
    }


    //ajax outbox
    public function ajaxOutbox(Request $request){

        $columns = array(
            0 => 'created',
            1 => 'msisdn',
            2 => 'status',
            3 => 'message',
        );


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');



        $totalData=Outbox::count();

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {

            $posts=Outbox::orderBy($order, $dir)
                ->offset($start)
                ->limit($limit)
                ->get();


        }else{

            $search = $request->input('search.value');


            $posts=Outbox::where('msisdn','LIKE',"%{$search}%")
                ->orWhere('message', 'LIKE', "%{$search}%")
                ->orWhere('created', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
               ->orderBy($order, $dir)
                ->get();


            $totalFiltered=Outbox::count();



        }


        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {


                $nestedData['message_date'] = $post->created;
                $nestedData['message'] = $post->message;
                $nestedData['status'] = $post->status;
                $nestedData['msisdn'] = $post->msisdn;


                $data[] = $nestedData;

            }

            $json_data = array(
                "draw" => intval($request->input('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data" => $data
            );

            echo json_encode($json_data);
        }
    }
}

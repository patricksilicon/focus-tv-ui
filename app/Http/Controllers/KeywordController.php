<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\KeywordType;
use App\Poll;
use App\Show;
use App\ShowKeyword;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KeywordController extends Controller
{
   public function index(){

       $keywords=Keyword::get();

       $keyword_types=KeywordType::get();

       $shows=Show::get();

       $polls=Poll::get();

       return view('keyword.keyword',compact('keywords','keyword_types','shows','polls'));
   }

   public function newKeyword(Request $request){
       try{

           $check=Keyword::where('keyword_name',$request->keyword)->first();

           if(!empty($check)){
               return redirect()->back()->with('error','Sorry keyword already exist');
           }

           $time=Carbon::now();
           //check for uniqueness
           $keyword=new Keyword();
           $keyword->keyword_name=trim($request->keyword);
           $keyword->type_id=$request->type;
           $keyword->callback_url=$request->url;
           $keyword->response=$request->response;
           $keyword->created=$time;
           $keyword->save();

           return redirect()->back()->with('success','successfully added');

       }catch (\Exception $exception){
           return redirect()->back()->with('error',$exception.'an error occurred');
       }
   }

   public function assignKeyword(Request $request){
       try{
           $assignKeyword=new ShowKeyword();
           $assignKeyword->show_id=$request->show;
           $assignKeyword->keyword_id=$request->keyword;
           $assignKeyword->save();


           return redirect()->back()->with('shows')->with('success','Successfully Assigned');

       }catch (\Exception $e){
           return redirect()->back()->with('error','Sorry went wrong');
       }
   }

   //more on show of a particular keyword
   public function shows($id){

       $showsid=ShowKeyword::where('keyword_id',$id)->get(['show_id']);

       $shows=Show::wherein('id',$showsid)->get();

       return $shows;
   }

   //more on show of a particular keyword
   public function keywordType($id){

//       $type_id=ShowKeyword::where('keyword_id',$id)->get(['type_id']);

       $keyword=Keyword::where('id',$id)->first();

       $type=KeywordType::where('id',$keyword->type_id)->get();

       return $type;
   }
}

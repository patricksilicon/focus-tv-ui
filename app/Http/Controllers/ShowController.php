<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\Show;
use App\ShowKeyword;
use App\ShowUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mockery\Exception;

class ShowController extends Controller
{
    public  function index(){

        try{
            $shows=Show::get();

            $users=User::where('roles_id',1)->get();

            $keywords=Keyword::get();

            return view('show.show', compact('shows','users','keywords'));

        }catch (\Exception $e){
            return $e;
        }

    }

    //show users of a particular show
    public function showUsers($show_id){

        try{
        $users_id=ShowUser::where('show_id',$show_id)->get(['user_id']);


        $users=User::wherein('id',$users_id)->get();

        return $users;

        }catch (\Exception $e){

           // return redirect()->back()->with('error','is an invalid number');

            return redirect()->to(route('home'))->with('error','dds');
        }
    }

    //show keywords of a particular show
    public function showKeywords($show_id){
        try{
            $keyword_id=ShowKeyword::where('show_id',$show_id)->get(['keyword_id']);


            $keywords=Keyword::wherein('id',$keyword_id)->get();

            return $keywords;

        }catch (\Exception $e){

            // return redirect()->back()->with('error','is an invalid number');

            return redirect()->to(route('home'))->with('error','dds');
        }
    }


    //create a new show
    public function newShow(Request $request){
        try{
            $time=Carbon::now();

            $show=new Show();
            $show->name=$request->name;
            $show->description=$request->description;
            $show->created=$time;
            $show->save();

            return redirect()->to(route('shows'))->with('success','Successfully created');

        }catch (\Exception $exception){

            return redirect()->to(route('home'))->with('error',$exception);
        }
    }

    //assign show to a presenter
    public function assignUserShow(Request $request){
        try{

        $check=ShowUser::where([['user_id',$request->user],['show_id',$request->show]])->first();

        if(empty($check)){
            $assign=new ShowUser();
            $assign->show_id=$request->show;
            $assign->user_id=$request->user;
            $assign->save();

            return redirect()->to(route('shows'))->with('success','Successfully created');
        }else{
            return redirect()->back()->with('error','Already assigned');
        }
    }
    catch (\Exception $e){
        return redirect()->back()->with('error','Sorry went wrong');
        }
    }

    //assign show keyword
    public function assignShowKeyword(Request $request){

        try{
            $assignKeyword=new ShowKeyword();
            $assignKeyword->show_id=$request->show;
            $assignKeyword->keyword_id=$request->keyword;
            $assignKeyword->save();

            return redirect()->to(route('shows'))->with('success','Successfully created');

        }catch (\Exception $e){
            return redirect()->back()->with('error',$e.'Sorry went wrong');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\KeywordType;
use App\Poll;
use App\PollAnswer;
use App\PollResult;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PollController extends Controller
{
   public function index(){

       $polls=Poll::get();

       //get poll keywords
       $type=KeywordType::where('type_name','poll')->first();

       $keywords=Keyword::where('type_id',$type->id)->get();

       return view('poll.poll',compact('polls','keywords'));
   }

   public function newPoll(Request $request){
    try{

       // return $request->daterange;

        $time=Carbon::now();
        //check if kewyord is already assign to a poll
        $check=Poll::where('keyword_id',$request->keyword)->first();

        if(!empty($check)){
            return redirect()->back()->with('error','Keyword Already in use');
        }

        $poll=new Poll();
        $poll->name=$request->poll;
        $poll->keyword_id=$request->keyword;
        $poll->description=$request->description;
        $poll->start_time=$request->start;
        $poll->created=$time;
        $poll->save();

        return redirect()->back()->with('success','Poll created');

    }catch (\Exception $e){
        return redirect()->back()->with('error','Error encountered when creating poll');
    }

   }

   //poll results
    public function pollResults($poll_id){

       return view('poll.poll-results',compact('poll_id'));
    }

    //get poll results
    public function getPollResult($poll_id){
//       $poll_id=1;

       $poll_answer_id=PollResult::where('poll_id',$poll_id)->get(['poll_answer_id']);

       $total=PollResult::where('poll_id',$poll_id)->count();

       $answer=PollAnswer::wherein('id',$poll_answer_id)->get();

       if(!empty($answer)){
           foreach ($answer as $ans){
               $results=PollResult::where([['poll_id',$poll_id],['poll_answer_id',$ans->id]])->count();

               $poll=new PollAnswer();
               $poll['answer']=$ans->answer;
               $poll['results']=($results/$total)*100;
               $poll1[]=$poll;
           }
       }

       return $poll1;
    }
}

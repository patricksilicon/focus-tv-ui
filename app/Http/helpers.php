<?php

  function checkRole($permissions){
    $userAccess = getMyPermission(auth()->user()->roles_id);
    foreach ($permissions as $key => $value) {
      if($value == $userAccess){
        return true;
      }
    }
    return false;
  }

  function getMyPermission($id)
  {
    switch ($id) {
      case 1:
        return 'Admin';
        break;
      case 2:
        return 'SuperAdmin';
        break;
      default:
        return 'Presenter';
        break;
    }
  }

?>

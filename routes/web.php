<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@start'
]);

Route::get('/forbidden', function (){
    return view('errors.403');
});

Route::get('404', [
    'uses' => 'HomeController@error404','as'=>'404',
]);

Route::get('405', [
    'uses' => 'HomeController@error405','as'=>'405',
]);


Route::any('signIn', [
    'uses' => 'AuthController@signIn','as'=>'signIn',
]);

Auth::routes();

Route::group(['middleware'=>'auth'], function () {

  //  Route::get('/home', 'HomeController@index')->name('home');

    Route::get('home', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'HomeController@index','as'=>'home',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);

    Route::get('inbox', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'InboxController@index','as'=>'inbox',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);

    Route::get('outbox', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'InboxController@outbox','as'=>'outbox',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);

    Route::any('ajax-outbox', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'InboxController@ajaxOutbox','as'=>'ajax-outbox',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);

    Route::any('ajax-inbox', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'InboxController@ajaxInbox','as'=>'ajax-inbox',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);


    Route::get('shows', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'ShowController@index','as'=>'shows',
        'roles' => ['Admin', 'SuperAdmin'] // Only an admin, or a super-admin can access this route
    ]);

    Route::get('show-users/{id}', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'ShowController@showUsers','as'=>'show-users',
        'roles' => ['Admin', 'SuperAdmin'] // Only an admin, or a super-admin can access this route
    ]);

    Route::get('show-keywords/{id}', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'ShowController@showKeywords','as'=>'show-keywords',
        'roles' => ['Admin', 'SuperAdmin'] // Only an admin, or a super-admin can access this route
    ]);


    Route::get('create-show', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'ShowController@newShow','as'=>'create-show',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('assign-show-user', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'ShowController@assignUserShow','as'=>'assign-show-user',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('assign-show-keyword', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'ShowController@assignShowKeyword','as'=>'assign-show-keyword',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('keywords', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'KeywordController@index','as'=>'keywords',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('new-keyword', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'KeywordController@newKeyword','as'=>'new-keyword',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('assign-keyword-show', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'KeywordController@assignKeyword','as'=>'assign-keyword-show',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('keyword-info-show/{id}', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'KeywordController@shows','as'=>'keyword-info-show',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('keyword-info-type/{id}', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'KeywordController@keywordType','as'=>'keyword-info-type',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('poll', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'PollController@index','as'=>'poll',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('new-poll', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'PollController@newPoll','as'=>'new-poll',
        'roles' => ['Admin', 'SuperAdmin']
    ]);

    Route::get('poll-result/{poll_id}', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'PollController@pollResults','as'=>'poll-result',
        'roles' => ['Admin', 'SuperAdmin','Presenter']
    ]);
    Route::get('poll-get-result/{poll_id}', [
        'middleware' => ['auth', 'roles'],
        'uses' => 'PollController@getPollResult','as'=>'poll-get-result',
        'roles' => ['Admin', 'SuperAdmin','Presenter']
    ]);

    Route::get('users', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'HomeController@index',
        'roles' => ['SuperAdmin'] // Only a super-admin can access this route
    ]);


    Route::get('ticker', [
        'middleware' => ['auth', 'roles'], // A 'roles' middleware must be specified
        'uses' => 'TestController@showTicker',
        'roles' => ['Presenter', 'Admin','SuperAdmin'] // Only an presenters,admin, or a super-admin can access this route
    ]);
});


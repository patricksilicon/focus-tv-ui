@extends('layouts.nav')
@section('content')
    @include('partials.response')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Inbox</h4>
                    {{--<h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>--}}
                    <div class="row">

                        <div class="col-3"></div>
                        <div class="col-1">Keyword</div>
                        <div class="col-3">
                            <select id="keyword" class="form-control ">
                                <option value="all">All Keywords</option>
                                @foreach($keywords as $keyword)
                                    <option value="{{$keyword->id}}">{{$keyword->keyword_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-1">Date</div>
                        <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                    <input type="text" class="form-control"
                                           id="date" placeholder="dd-mm-yyyy">

                                </div>

                            {{--<select class="form-control">--}}
                                {{--<option>All</option>--}}
                                {{--<option>Today</option>--}}
                                {{--<option>Yesterday</option>--}}
                            {{--</select>--}}
                        </div>
                    </div>

                    <div class="table-responsive m-t-40">


                        <table id="inbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>DATE SENT</th>
                                <th>MSISDN</th>
                                <th width="70%">TEXT</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>DATE SENT</th>
                                <th>MSISDN</th>
                                <th width="70%">TEXT</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            {{--@foreach($inbox as $message)--}}
                            {{--<tr>--}}
                            {{--<td>{{$message->message_date}}</td>--}}
                            {{--<td>{{$message->msisdn}}</td>--}}
                            {{--<td width="70%">{{$message->message}}</td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@extends('layouts.nav')
@section('content')
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">OutBox</h4>
                            {{--<h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>--}}
                            <div class="table-responsive m-t-40">
                                <table id="outbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>

                                        <th>DATE SENT</th>
                                        <th>MSISDN</th>
                                        <th>STATUS</th>
                                        <th  width="50%">TEXT</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>DATE SENT</th>
                                        <th>MSISDN</th>
                                        <th>STATUS</th>
                                        <th  width="50%">TEXT</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    {{--@foreach($outbox as $message)--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$message->message_date}}</td>--}}
                                            {{--<td>{{$message->msisdn}}</td>--}}
                                            {{--<td width="70%">{{$message->message}}</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


@endsection

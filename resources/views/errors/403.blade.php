@extends('layouts.name')
@section('content')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body" style="text-align: center">
                                <h1 style="color: black">Access Denied</h1>
                                <h2>This Could be due to:<br>
                                    <h3><br>
                                        You Tried to Access a page that is above your Access-Level.<br>
                                        <br>
                                    </h3>
                                    If this was an error, Contact Admin for Assistance.
                                </h2>
                                <br>
                                Email: <a href="mailto:info@epartograph.org">admin@siliconsolutions.co.ke</a><br><br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

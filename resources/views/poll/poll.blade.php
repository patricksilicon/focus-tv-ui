@extends('layouts.nav')
@section('content')
    <div class="row">
        @include('partials.response')
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Polls</h4>

                    <div class="row">
                        <div class="col-lg-10 col-md-6 col-sm-6 col-xs-4 pull-right "></div>
                        <div class="mdi mdi-plus btn btn-info pull-right" data-toggle="modal"
                             data-target="#new-poll">new Poll
                        </div>
                    </div>
                    <div class="table-responsive m-t-40">
                        <table id="polls" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Added on</th>
                                <th>NAME</th>
                                <th>STATUS</th>
                                <th>START </th>
                                <th>END </th>
                                <th>ACTIONS</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Added on</th>
                                <th>NAME</th>
                                <th>STATUS</th>
                                <th>START </th>
                                <th>END </th>
                                <th>ACTIONS</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($polls as $poll)
                                <tr>
                                    <td>{{$poll->created}}</td>
                                    <td>{{$poll->name}}</td>
                                    <td>{{$poll->status}}</td>
                                    <td>{{$poll->start_time}}</td>
                                    <td>{{$poll->end_time}}</td>
                                    <td>
                                        <button class="btn btn-error text-center ">
                                            <span class="mdi mdi-pen"></span> edit
                                        </button>&af;
                                        <button class="btn btn-danger ">
                                            <span class="mdi mdi-delete"></span>delete
                                        </button>
                                        <a href="{{route('poll-result',$poll->id)}}"> <button id="{{$poll->id}}" class="btn btn-info more-info-keyword">
                                            <span class="mdi mdi-view-dashboard"></span>more
                                            </button></a>

                                    </td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('partials.poll.new-poll')
@endsection
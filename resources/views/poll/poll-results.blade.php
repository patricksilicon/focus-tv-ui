@extends('layouts.nav')
@section('content')
    <div class="row">
        @include('partials.response')


        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <input id="poll_id" type="hidden" value="{{$poll_id}}">
                    <h4 class="card-title">Bar Chart</h4>
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
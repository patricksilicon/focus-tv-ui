{{--modal--}}

<div id="new-keyword" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Keyword</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal" action="{{route('new-keyword')}}">
                <div class="modal-body">

                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="type" class="control-label">Type:</label>
                        <select name="type" class="form-control" id="type" required>
                            <option disabled selected>select type</option>
                            @foreach($keyword_types as $type)
                                <option value="{{$type->id}}" >{{$type->type_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="keyword" class="control-label">Keyword</label>
                        <input  name="keyword" class="form-control" id="name" required>
                    </div>

                    <div class="form-group">
                        <label for="url" class="control-label">Callback Url</label>
                        <input name="url" class="form-control" id="url" >
                    </div>

                    <div class="form-group">
                        <label for="response" class="control-label">Response</label>
                        <textarea class="form-control" name="response" id="response" required ></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--modal--}}

<div id="keyword-assign-show{{$keyword->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Assign keyword to show </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{route('assign-keyword-show')}}">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Keyword:</label>
                        <input class="form-control" id="name" value="{{$keyword->keyword_name}}" disabled>
                        <input value="{{$keyword->id}}" name="keyword" type="hidden" class="form-control" id="">
                    </div>

                    <div class="form-group">
                        <label for="shows" class="control-label">Show:</label>
                        <select  class="form-control" id="shows" name="show" required>
                            <option  disabled  selected >Select Show</option>
                            @foreach($shows as $show)
                                <option value="{{$show->id}}">{{$show->name}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--modal--}}

<div id="assign-presenter{{$show->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Assign show to presenter</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{route('assign-show-user')}}">
                     {{csrf_field()}}
            <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Show:</label>
                        <input class="form-control" id="name" value="{{$show->name}}" disabled>
                        <input name="show"   value="{{$show->id}}"  type="hidden" class="form-control" >
                    </div>

                    <div class="form-group">
                        <label for="presenter" class="control-label">Presenter:</label>
                        <select  class="form-control" id="presenter" name="user" required>
                            <option  disabled selected >Select presenter</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->email}}</option>
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger waves-effect waves-light">Assign</button>
            </div>
            </form>
        </div>
    </div>
</div>

{{--modal--}}

<div id="new-show" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Show</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal" action="{{route('create-show')}}">
            <div class="modal-body">

                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name" class="control-label">Name</label>
                        <input name="name" class="form-control" id="name" required>
                    </div>

                    <div class="form-group">
                        <label for="description" class="control-label">Description</label>
                       <textarea class="form-control" name="description" id="description" required></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger waves-effect waves-light">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>

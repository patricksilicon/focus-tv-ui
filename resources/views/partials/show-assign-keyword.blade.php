{{--modal--}}

<div id="assign-keyword{{$show->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Assign show keyword</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{route('assign-show-keyword')}}">
                {{csrf_field()}}
            <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="control-label">Show:</label>
                        <input class="form-control" id="name" value="{{$show->name}}" disabled>
                        <input value="{{$show->id}}" name="show" type="hidden" class="form-control" id="">
                    </div>

                    <div class="form-group">
                        <label for="keyword" class="control-label">Keyword:</label>
                        <select  class="form-control" id="keyword" name="keyword" required>
                            <option  disabled  selected >Select keyword</option>
                            @foreach($keywords as $keyword)
                                <option value="{{$keyword->id}}">{{$keyword->keyword_name}}</option>
                            @endforeach
                        </select>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger waves-effect waves-light">Assign</button>
            </div>
            </form>
        </div>
    </div>
</div>

{{--modal--}}

<div id="new-poll" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Poll</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form class="form-horizontal" action="{{route('new-poll')}}">
                <div class="modal-body">

                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="keyword" class="control-label">Keyword:</label>
                        <select name="keyword" class="form-control" id="keyword" required>
                            <option disabled selected>select type</option>
                            @foreach($keywords as $keyword)
                                <option value="{{$keyword->id}}" >{{$keyword->keyword_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="name" class="control-label">Name</label>
                        <input  name="poll" class="form-control" id="name" required>
                    </div>
                    <div class="form-group">
                        <label for="date" class="control-label">Test</label>
                        <input type="text" class="form-control input-daterange-timepicker"
                               name="daterange" >
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="date" class="control-label">Start</label>--}}
                        {{--<input type="text" name="start" class="form-control" id="date" placeholder="yyyy-mm-dd">--}}
                    {{--</div>--}}

                    {{----}}

                    {{--<div class="form-group">--}}
                        {{--<label for="date" class="control-label">End</label>--}}
                        {{--<input type="text" name="end" class="form-control" id="end_date" placeholder="yyyy-mm-dd">--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="description" class="control-label">Description</label>
                        <textarea class="form-control" name="description" id="description" required ></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

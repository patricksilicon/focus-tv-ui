

<div id="more-info" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> More info</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form><input id="show_id" hidden value="{{$show->id}}"></form>

                    <div class="col-6">
                        Users
                        <div id="more-info-users"></div>
                        {{--<input id="more-info-table">--}}

                    </div>

                    <div class="col-6">
                        Keywords
                        <div id="more-info-keywords"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!DOCTYPE html>
<html lang="en">

<head>

    <script>
        window.onload = function () {



        }
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="http://focustv.co.ke/wp-content/uploads/2016/11/Focus-Tv-Logo.png">
    <title>Focus Tv</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">

    {{--date pickers--}}
    <link href="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('css/colors/default.css')}}" id="theme" rel="stylesheet">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-85622565-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body class="fix-header fix-sidebar card-no-border">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">
                    <!-- Logo icon -->
                    <b>
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                        <!-- Light Logo icon -->
                        <img src="../assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                    </b>
                    <!--End Logo icon -->
                    <!-- Logo text -->
                    <span>
                         <!-- dark Logo text -->
                         <img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                        <!-- Light Logo text -->
                         <img src="../assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav mr-auto mt-md-0 ">
                    <!-- This is  -->
                    <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                    <!-- ============================================================== -->
                    <!-- Comment -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                            <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                        </a>
                        <div class="dropdown-menu mailbox animated bounceInDown">
                            <ul>
                                <li>
                                    <div class="drop-title">Notifications</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-email"></i>
                            <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                        </a>
                        <div class="dropdown-menu mailbox animated bounceInDown" aria-labelledby="2">
                            <ul>
                                <li>
                                    <div class="drop-title">You have 4 new messages</div>
                                </li>
                                <li>
                                    <div class="message-center">
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"> <img src="../assets/images/users/1.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"> <img src="../assets/images/users/2.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"> <img src="../assets/images/users/3.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                        </a>
                                        <!-- Message -->
                                        <a href="#">
                                            <div class="user-img"> <img src="../assets/images/users/4.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                        </a>
                                    </div>
                                </li>
                                <li>
                                    <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>

                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item hidden-sm-down">
                        <form class="app-search">
                            <input type="text" class="form-control" placeholder="Search for..."> <a class="srh-btn"><i class="ti-search"></i></a> </form>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img"><img src="../assets/images/users/1.jpg" alt="user"></div>
                                        <div class="u-text">
                                            <h4>Steave Jobs</h4>
                                            <p class="text-muted">varun@gmail.com</p><a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li class="nav-small-cap">PERSONAL</li>
                    <li>
                        <a class="has-arrow" href="{{route('home')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                    </li>


                    <li>
                        <a class="has-arrow " href="{{route('inbox')}}" aria-expanded="false"><i class="mdi mdi-message-processing"></i><span class="hide-menu">Inbox</span></a>
                    </li>
                    <li>
                        <a class="has-arrow " href="{{route('outbox')}}" aria-expanded="false"><i class="mdi mdi-message-reply"></i><span class="hide-menu">Oubox</span></a>
                    </li>

                    {{--<li>--}}
                    {{--<a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Bulk</span></a>--}}
                    {{--<ul aria-expanded="false" class="collapse">--}}
                    {{--<li><a href="app-calendar.html">Send Bulk</a></li>--}}
                    {{--<li><a href="Test">Outbox</a></li>--}}
                    {{--<li><a href="app-calendar.html">Scheduled Bulk</a></li>--}}

                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a class="has-arrow " aria-expanded="false"><i class="mdi mdi-phone"></i><span class="hide-menu">Contacts</span></a>--}}
                    {{--<ul aria-expanded="false" class="collapse">--}}
                    {{--<li><a href="app-calendar.html">PhoneBook</a></li>--}}
                    {{--<li><a href="app-calendar.html">All Contact</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-key"></i><span class="hide-menu">KeyWord</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{route('keywords')}}">Keywords</a></li>
                            {{--<li><a href="ui-tooltip-stylish.html">Create keyword</a></li>--}}
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Show</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{route('shows')}}">Shows</a></li>
                            <li><a href="ui-tooltip-stylish.html">Assign Presenter</a></li>
                        </ul>
                    </li>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Polls</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{route('poll')}}">Polls</a></li>
                            {{--<li><a href="ui-tooltip-stylish.html">Create Poll</a></li>--}}
                        </ul>
                    </li>


                    {{--<li>--}}
                        {{--<a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Ticker</span></a>--}}
                        {{--<ul aria-expanded="false" class="collapse">--}}
                            {{--<li><a href="ui-tooltip-stylish.html">Ticker</a></li>--}}
                            {{--<li><a href="ui-tooltip-stylish.html">New Ticker</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    {{--@if(checkRole(['Admin']))--}}

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-account-box"></i><span class="hide-menu">Users</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="ui-tooltip-stylish.html">Users</a></li>
                            <li><a href="ui-tooltip-stylish.html">Register Presenter</a></li>
                        </ul>
                    </li>
                    {{--@endif--}}
                </ul>
            </nav>

        </div>

    </aside>
    <div class="page-wrapper">
        <div class="container-fluid">

            @yield('content')
            <footer class="footer">
                © 2018 Focus Tv
            </footer>
        </div>
    </div>


</div>

<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('assets/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->

{{--<script src="{{asset('js/jquery.slimscroll.js')}}"></script>--}}
<!--Wave Effects -->
<script src="{{asset('js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('js/sidebarmenu.js')}}"></script>
<!--stickey kit -->

<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script src="{{asset('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>

<script src="{{asset('js/custom.min.js')}}"></script>

<script src="{{asset('assets/plugins/chartist-js/dist/chartist.min.js')}}"></script>
<script src="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>

<script src="{{asset('assets/plugins/echarts/echarts-all.js')}}"></script>

<script src="{{asset('js/dashboard1.js')}}"></script>



<script src="{{asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>

    //get all info about show
    $(".more-info-button").click(function(){
        var show = $(this).attr("id");

        $('#more-info-users').find('input').remove();
        $('#more-info-keywords').find('input').remove();

        $('#more-info').modal('show');
        //get show users
        $.getJSON("show-users/"+show,
            function(j){

                for (var i = 0; i < j.length; i++) {
                    var email = j[i]['email'];

                    $("#more-info-users").append("<input disabled style='color: black;' class='form-contro' name='name'   value='"+email+"'>");

                }
            })
        //get show users
        $.getJSON("show-keywords/"+show,
            function(j){
                for (var i = 0; i < j.length; i++) {
                    var keyword = j[i]['keyword_name'];

                    $("#more-info-keywords").append("<input disabled style='color: black;' class='form-contro' name='name'   value='" + keyword + "'>");

                }
            })
    });

    //get more info about keyword
    $(".more-info-keyword").click(function(){
        var keyword = $(this).attr("id");

        $('#more-info-type').find('input').remove();
        $('#more-info-keyword-show').find('input').remove();

        $('#keyword-info').modal('show');

        //get keyword type
        $.getJSON("keyword-info-type/"+keyword,
            function(j){
                for (var i = 0; i < j.length; i++) {
                    var type = j[i]['type_name'];

                    $("#more-info-type").append("<input disabled style='color: black;' class='form-contro' name='name'   value='" + type + "'>");

                }
            })

        //get keyword shows
        $.getJSON("keyword-info-show/"+keyword,
            function(j){

                for (var i = 0; i < j.length; i++) {
                    var name = j[i]['name'];

                    $("#more-info-keyword-show").append("<input disabled style='color: black;' class='form-contro' name='name'   value='"+name+"'>");

                }
            })

    });



    jQuery('#date').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    jQuery('#end_date').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'YYYY/MM/DD h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });

    $(document).ready(function() {





        // $('#inbox').DataTable({
        //     "order": [[ 0, "desc" ]],
        // });

        $('#show').DataTable({
            "order": [[ 0, "desc" ]],
        });

        $('#polls').DataTable({
            "order": [[ 0, "desc" ]],
        });

        $('#inbox').DataTable({
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "ajax":{
                "url": "{{ url('ajax-inbox') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", keyword: "all", date:"null"}
            },
            "columns": [
                { "data": "message_date" },
                { "data": "msisdn" },
                { "data": "message" },

            ],
            lengthMenu: [
                [10,50, 100,500, 1000,1000000000],
                [ '10','50', '100','500', '1000', 'All' ]
            ],
            "order": [[ 0, "desc" ]],

        });


        $('#outbox').DataTable({
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "ajax":{
                "url": "{{ url('ajax-outbox') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
            },
            "columns": [
                { "data": "message_date" },
                { "data": "msisdn" },
                { "data": "status" },
                { "data": "message" },

            ],
            lengthMenu: [
                [10,50, 100,500, 1000,1000000000],
                [ '10','50', '100','500', '1000', 'All' ]
            ],
            "order": [[ 0, "desc" ]],

        });

    });

    $('#keyword').change(function () {
        var table = $('#inbox').DataTable();
        var keyword=$(this).val();
        var date=$('#date').val();

        if(date==""){
            date="null";
        }
        // table
        //     .clear()
        //     .draw();

        table.destroy();

        $('#inbox').DataTable({
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "ajax":{
                "url": "{{ url('ajax-inbox') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", keyword: keyword, date: date}
            },
            "columns": [
                { "data": "message_date" },
                { "data": "msisdn" },
                { "data": "message" },

            ],
            lengthMenu: [
                [10,50, 100,500, 1000,1000000000],
                [ '10','50', '100','500', '1000', 'All' ]
            ],
            "order": [[ 0, "desc" ]],

        });
    })

    $('#date').change(function () {
        var table = $('#inbox').DataTable();
        var date=$(this).val();
        var keyword=$('#keyword').val();
        // table
        //     .clear()
        //     .draw();

        if(date==""){
            date="null";
        }

        table.destroy();

        $('#inbox').DataTable({
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "ajax":{
                "url": "{{ url('ajax-inbox') }}",
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", keyword: keyword, date: date}
            },
            "columns": [
                { "data": "message_date" },
                { "data": "msisdn" },
                { "data": "message" },

            ],
            lengthMenu: [
                [10,50, 100,500, 1000,1000000000],
                [ '10','50', '100','500', '1000', 'All' ]
            ],
            "order": [[ 0, "desc" ]],

        });
    })

    //    alert
    //     $(".myadmin-alert .closed").click(function(){
    //         $.toast({
    //             heading: 'Welcome to Monster admin',
    //             text: 'Use the predefined ones, or specify a custom position object.',
    //             position: 'top-right',
    //             loaderBg:'#ff6849',
    //             icon: 'warning',
    //             hideAfter: 3500,
    //             stack: 6
    //         });
    //
    //     });

</script>

{{--<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>--}}
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script src="{{asset('assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>

<script src="{{asset('ownjs/poll-results.js')}}"></script>
</body>

</html>
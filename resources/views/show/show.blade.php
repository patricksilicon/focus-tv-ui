@extends('layouts.nav')
@section('content')
    <div class="row">
        @include('partials.response')
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Shows</h4>

                    <div class="row ">
                        <div class="col-11 pull-right "></div>
                        <span class="mdi mdi-plus btn btn-info" data-toggle="modal"
                              data-target="#new-show">new show</span>
                    </div>

                    <div class="table-responsive m-t-40">
                        <table id="show" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Added on</th>
                                <th>NAME</th>
                                <th>DESCRIPTION</th>
                                <th>ACTIONS</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Added on</th>
                                <th>NAME</th>
                                <th>DESCRIPTION</th>
                                <th>ACTIONS</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($shows as $show)
                                <tr>
                                    <td>{{$show->created}}</td>
                                    <td>{{$show->name}}</td>
                                    <td>{{$show->description}}</td>
                                    <td>
                                        <button class="btn btn-error text-center ">
                                            <span class="mdi mdi-pen"></span> edit
                                        </button>&af;
                                        <button class="btn btn-danger ">
                                            <span class="mdi mdi-delete"></span>delete
                                        </button>
                                        <button id="{{$show->id}}" class="btn btn-info more-info-button"
                                        >
                                            <span class="mdi mdi-view-dashboard"></span>more
                                        </button>

                                        <div class="btn-group">
                                            {{--<button type="button" class="btn btn-danger">Assign</button>--}}
                                            <button type="button"
                                                    class="btn btn-success dropdown-toggle dropdown-toggle-split"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Assign
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" data-toggle="modal"
                                                   data-target="#assign-presenter{{$show->id}}">To Presenter</a>
                                                <a class="dropdown-item" data-toggle="modal"
                                                   data-target="#assign-keyword{{$show->id}}">Keyword </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @include('partials.show-info-modal')
                                @include('partials.assign-to-presenter')
                                @include('partials.show-assign-keyword')
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.new-show')
@endsection
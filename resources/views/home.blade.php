@extends('layouts.nav')
@section('content')

    @include('partials.response')
        <div class="row">
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Total Inbox</h4>
                            <div class="text-right">
                                <h2 class="font-light m-b-0"><i class="ti-arrow-up text-success"></i> 12000 sms</h2>
                                {{--<span class="text-muted">Total Inbox</span>--}}
                            </div>
                            <span class="text-success">80%</span>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Todays Inbox</h4>
                            <div class="text-right">
                                <h2 class="font-light m-b-0"><i class="ti-arrow-up text-info"></i> 5,000 sms</h2>
                                {{--<span class="text-muted">Todays Income</span>--}}
                            </div>
                            <span class="text-info">30%</span>
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">No. of Shows</h4>
                            <div class="text-right">
                                <h2 class="font-light m-b-0"><i class="ti-arrow-up text-purple"></i> 8</h2>
                                {{--<span class="text-muted">Todays Income</span>--}}
                            </div>
                            <span class="text-purple">60%</span>
                            <div class="progress">
                                <div class="progress-bar bg-purple" role="progressbar" style="width: 60%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Keywords</h4>
                            <div class="text-right">
                                <h2 class="font-light m-b-0"><i class="ti-arrow-down text-danger"></i> 12</h2>
                                {{--<span class="text-muted">Todays Income</span>--}}
                            </div>
                            <span class="text-danger">80%</span>
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>

            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="d-flex flex-wrap">
                                        <div>
                                            <h3>Revenue Statistics</h3>
                                            <h6 class="card-subtitle">January 2017</h6> </div>
                                        <div class="ml-auto ">
                                            <ul class="list-inline">
                                                <li>
                                                    <h6 class="text-muted"><i class="fa fa-circle m-r-5 text-success"></i>Product A</h6> </li>
                                                <li>
                                                    <h6 class="text-muted"><i class="fa fa-circle m-r-5 text-info"></i>Product B</h6> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="total-revenue4" style="height: 350px;"></div>
                                </div>
                                <div class="col-lg-3 col-md-6 m-b-30 m-t-20 text-center">
                                    <h1 class="m-b-0 font-light">$54578</h1>
                                    <h6 class="text-muted">Total Revenue</h6></div>
                                <div class="col-lg-3 col-md-6 m-b-30 m-t-20 text-center">
                                    <h1 class="m-b-0 font-light">$43451</h1>
                                    <h6 class="text-muted">Online Revenue</h6></div>
                                <div class="col-lg-3 col-md-6 m-b-30 m-t-20 text-center">
                                    <h1 class="m-b-0 font-light">$44578</h1>
                                    <h6 class="text-muted">Product A</h6></div>
                                <div class="col-lg-3 col-md-6 m-b-30 m-t-20 text-center">
                                    <h1 class="m-b-0 font-light">$12578</h1>
                                    <h6 class="text-muted">Product B</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endsection
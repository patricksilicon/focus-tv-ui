@extends('layouts.nav')
@section('content')
    <div class="row">
        @include('partials.response')
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Keywords</h4>

                    <div class="row">
                        <div class="col-lg-10 col-md-6 col-sm-6 col-xs-4 pull-right "></div>
                        <div class="mdi mdi-plus btn btn-info pull-right" data-toggle="modal"
                             data-target="#new-keyword">new keyword
                        </div>
                    </div>


                    <div class="table-responsive m-t-40">
                        <table id="show" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Added on</th>
                                <th>NAME</th>
                                <th>CALL-BACK-URL</th>
                                <th>RESPONSE</th>
                                <th>ACTIONS</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Added on</th>
                                <th>NAME</th>
                                <th>CALL-BACK-URL</th>
                                <th width="30%">RESPONSE</th>
                                <th>ACTIONS</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($keywords as $keyword)
                                <tr>
                                    <td>{{$keyword->created}}</td>
                                    <td>{{$keyword->keyword_name}}</td>
                                    <td>{{$keyword->callback_url}}</td>
                                    <td width="30%">{{$keyword->response}}</td>
                                    <td>
                                        <button class="btn btn-error text-center ">
                                            <span class="mdi mdi-pen"></span> edit
                                        </button>&af;
                                        <button class="btn btn-danger ">
                                            <span class="mdi mdi-delete"></span>delete
                                        </button>
                                        <button id="{{$keyword->id}}" class="btn btn-info more-info-keyword"
                                        >
                                            <span class="mdi mdi-view-dashboard"></span>more
                                        </button>

                                        <div class="btn-group">
                                            {{--<button type="button" class="btn btn-danger">Assign</button>--}}
                                            <button type="button"
                                                    class="btn btn-success dropdown-toggle dropdown-toggle-split"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Assign
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" data-toggle="modal"
                                                   data-target="#keyword-assign-show{{$keyword->id}}">To Show</a>
                                                <a class="dropdown-item" data-toggle="modal"
                                                   data-target="#keyword-assign-poll{{$keyword->id}}">To Poll </a>
                                                {{--<a class="dropdown-item" data-toggle="modal"--}}
                                                   {{--data-target="#assign-keyword{{$keyword->id}}">To Promo </a>--}}
                                                {{--<a class="dropdown-item" data-toggle="modal"--}}
                                                   {{--data-target="#assign-keyword{{$keyword->id}}">To Quiz </a>--}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @include('partials.keyword.keyword-assign-show')
                                @include('partials.keyword.keyword-assign-poll')
                                @include('partials.keyword.show-info-keyword')
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.keyword.new-keyword')
@endsection